locals {
  all_api_method_response_codes = [
    for k, v in var.api_methods : {
      "${k}" = v["response_models"]
    }
  ]
  all_api_method_response_codes_map = [
    for apiObject in local.all_api_method_response_codes :
    [
      for apiName, apiResponse in apiObject :
      [
        for responseCode, responseBody in apiResponse :
        {
          "responseCode" = responseCode,
          "apiName"      = apiName,
          "responseBody" = responseBody
        }
      ]
    ]
  ]
  flattened_all_api_method_response_codes_map = flatten(local.all_api_method_response_codes_map)
  api_method_response_codes_map = merge(
    {
      for k, v in local.flattened_all_api_method_response_codes_map :
      "${v.apiName}_${v.responseCode}" => v
    }
  )
}