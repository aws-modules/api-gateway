resource "aws_api_gateway_rest_api" "this" {
  name               = var.application != null ? "${var.environment}_${var.product}_${var.application}_${var.use_case}" : "${var.environment}_${var.product}_${var.use_case}"
  binary_media_types = var.binary_media_types
  api_key_source     = var.api_key_source
  tags = var.application != null ? {
    Environment             = var.environment
    Product                 = var.product
    Application             = var.application
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
    } : {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
  endpoint_configuration {
    types = var.endpoint_configuration
  }
}

resource "aws_api_gateway_resource" "this" {
  for_each    = var.api_resources
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = each.value.parent_resource_id == "root" ? aws_api_gateway_rest_api.this.root_resource_id : each.value.parent_resource_id
  path_part   = each.value.resource_name
}

resource "aws_api_gateway_method" "this" {
  for_each         = var.api_methods
  rest_api_id      = aws_api_gateway_rest_api.this.id
  resource_id      = each.value.resource_name == "NULL" ? aws_api_gateway_rest_api.this.root_resource_id : aws_api_gateway_resource.this[each.value.tf_resource_id].id
  http_method      = each.value.http_method
  authorization    = each.value.authorization
  authorizer_id    = each.value.authorizer_id
  api_key_required = each.value.api_key_required
  request_models   = lookup(each.value, "request_model", null) != null ? { "application/json" : aws_api_gateway_model.this[each.value.request_model].name } : {}
}

resource "aws_api_gateway_integration" "this" {
  for_each                = var.api_methods
  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = each.value.resource_name == "NULL" ? aws_api_gateway_rest_api.this.root_resource_id : aws_api_gateway_resource.this[each.value.tf_resource_id].id
  http_method             = aws_api_gateway_method.this[each.key].http_method
  integration_http_method = "POST"      // Should be POST for AWS Lambda. Refer : https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
  type                    = "AWS_PROXY" // Should be AWS_PROXY for AWS Lambda. Refer : https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
  uri                     = each.value.function_invoke_arn
}

resource "aws_lambda_permission" "this" {
  for_each      = var.api_methods
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = each.value.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = each.value.resource_name == "NULL" ? "${aws_api_gateway_rest_api.this.execution_arn}/*/*/" : "${aws_api_gateway_rest_api.this.execution_arn}/*/*/${each.value.resource_name}"
}

module "api-gateway-enable-cors" {
  for_each        = var.api_resources
  source          = "squidfunk/api-gateway-enable-cors/aws"
  version         = "0.3.3"
  api_id          = aws_api_gateway_rest_api.this.id
  api_resource_id = aws_api_gateway_resource.this[each.key].id
  allow_origin    = lookup(each.value, "allow_origin", var.allow_origin)
  allow_headers   = lookup(each.value, "allow_headers", var.allow_headers)
  allow_methods   = lookup(each.value, "allow_methods", var.allow_methods)
}

locals {
  model_files = toset(fileset("${path.cwd}/${var.api_models_path}", "*"))
  model_file_names = [
    for file in local.model_files :
    trimsuffix(file, ".json")
  ]
}

resource "aws_api_gateway_model" "this" {
  for_each     = toset(local.model_file_names)
  rest_api_id  = aws_api_gateway_rest_api.this.id
  name         = each.key
  content_type = "application/json"
  schema       = file("${path.cwd}/${var.api_models_path}/${each.key}.json")
}

resource "aws_api_gateway_method_response" "this" {
  for_each        = local.api_method_response_codes_map
  rest_api_id     = aws_api_gateway_rest_api.this.id
  resource_id     = var.api_methods[each.value.apiName].resource_name == "NULL" ? aws_api_gateway_rest_api.this.root_resource_id : aws_api_gateway_resource.this[var.api_methods[each.value.apiName].tf_resource_id].id
  http_method     = aws_api_gateway_method.this[each.value.apiName].http_method
  response_models = { "application/json" : aws_api_gateway_model.this[each.value.responseBody].name }
  status_code     = each.value.responseCode
}